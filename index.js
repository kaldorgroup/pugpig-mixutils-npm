let fs = require('fs')
let path = require('path')

function getVersionTag() {  
  let {gitDescribeSync} = require('git-describe')
  const gitInfo = gitDescribeSync({match: '[0-9]*\.[0-9]*\.[0-9]*'})
  const gitTagVersion = (gitInfo.tag || '?') + (gitInfo.distance > 0 ? '_dev' : '') + (gitInfo.dirty ? '_local' : '')
  return gitTagVersion
}

function createGitTagFile(directory) {
  let tag = getVersionTag()
  fs.writeFile(path.join(directory, "gittag.txt"), tag, (err) => {
    if (err) {
      throw err;
    }
  });
}

function makePackage(src) {
  let name = path.basename(src)
  let parentdir = path.dirname(src)

  let zipFullFilename = path.join(
    path.dirname(src),
    'build',
    name + '.tar.gz',
  )
  console.log('Creating ', zipFullFilename, '...')

  let tar = require('tar');
  tar.c({
      gzip: true,
      file: zipFullFilename,
      sync: true,
      cwd: parentdir,
    }, [name])

  console.log('Created ' + zipFullFilename)
}

function getPluginToFixAssetPathsForHtml() {
  return new class {
      apply(compiler) {
        compiler.plugin('compilation', (compilation) => {
          compilation.plugin(
            'html-webpack-plugin-before-html-processing',
            (data) => {
              for (var i = 0; i<data.assets.css.length; i++) {
                data.assets.css[i] = '..' + data.assets.css[i]
              }
              for (var i = 0; i<data.assets.js.length; i++) {
                data.assets.js[i] = '..' + data.assets.js[i].substring(1)
              }
            }
          )
        })
      }
    }
  }

function copyVersionedAsset(assets, pattern, target) {
  let assetFilePaths = Object.keys(assets)
  for (var i = 0; i < assetFilePaths.length; i++) {
    if (assetFilePaths[i].match(pattern)) {
      let filename = assetFilePaths[i]
      let fullFilename = assets[filename].existsAt
      let outDirectory = path.dirname(fullFilename)

      fs.copyFileSync(fullFilename, path.join(outDirectory, target))
      console.log(`Copied [${filename}] to [${target}]`)
      return fullFilename
    }
  }
}

function moveVersionedAsset(assets, pattern, target) {
  let fullFilename = copyVersionedAsset(assets, pattern, target)
  if (fullFilename) {
    fs.unlinkSync(fullFilename, (err) => {
      if (err) {
        throw err
      }
      console.log(`Removed [${fullFilename}]`)
    })
  }
}

module.exports = {
  getVersionTag,
  createGitTagFile,
  copyVersionedAsset,
  moveVersionedAsset,
  makePackage,
  getPluginToFixAssetPathsForHtml
}